import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {App2} from './app2.component.ts';
import {enableProdMode} from '@angular/core';
import {APP_BASE_HREF} from "@angular/common";

enableProdMode();

@NgModule({
  imports: [
    BrowserModule
  ],
  providers: [{provide: APP_BASE_HREF, useValue: '/app2/'}],
  declarations: [
    App2
  ],
  bootstrap: [App2]
})
export default class MainModule {
}
