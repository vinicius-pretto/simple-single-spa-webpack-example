import {Component} from '@angular/core';

@Component({
  selector: 'app2',
  template: `
    <div style="margin-top: 100px;">
      <h1>Micro Frontends Angular 2</h1>
    </div>
  `,
})
export class App2 {
}
